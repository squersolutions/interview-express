const express = require('express');
const cors = require('cors');
const app = express();
const port = 1234;

app.use(cors());

app.get('/', (req, res) => {
  res.send(`{info: 'Connected to Express Service!'}`);
});

app.get('/hello', (req, res) => {
  res.send({ info: 'The Service says Hello World!' });
});

app.listen(port, () => {
  console.log(`Example app listening at http://localhost:${port}`);
});
